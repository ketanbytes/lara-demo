<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\BlogTags;
use Illuminate\Http\Request;
use Exception;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $blogs = Blog::with('allTags')->latest()->get();
            return view('blog.list', compact('blogs'));
        } catch (Exception $e) {
            return redirect()->back()->with(['status' => 'danger','message' => $e->getMessage()])->withInput();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $allTags = $this->getAllTags();
        return view('blog.form', compact('allTags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validatedData = $this->validate(
                $request,
                [
                    'title' => 'required|min:5|max:30',
                    'short_description' => 'required|min:10|max:80',
                    'description' => 'required',
                    'tags' => 'required',
                    'tags.*' => 'required'
                ]
            );

            $blog = Blog::create(
                [
                'title' => $request->title,
                'short_description' => $request->short_description,
                'description' => $request->description,
                ]
            );

            if ($request->tags) {
                $this->storeTags($request->tags, $blog->id);
            }

            return redirect()
                ->route('blog.index')
                ->with(['status' => 'success','message'=>'Blog created successfully !!']);
        } catch (Exception $e) {
            return redirect()->back()->with(['status' => 'danger','message' => $e->getMessage()])->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Blog $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        try {
            $blog = Blog::with('allTags')->where('id', $blog->id)->first();
            return view('blog.view', compact('blog'));
        } catch (Exception $e) {
            return redirect()->back()->with(['status' => 'danger','message' => $e->getMessage()])->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Blog $blog
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog)
    {
        try {
            $blog = Blog::with('allTags')->where('id', $blog->id)->first();
            if ($blog->allTags->toArray()) {
                $blog->selectedTags = $blog->allTags->pluck('tags')->toArray();
            }
            $allTags = $this->getAllTags();
            return view('blog.form', compact('blog', 'allTags'));
        } catch (Exception $e) {
            return redirect()->back()->with(['status' => 'danger','message' => $e->getMessage()])->withInput();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Blog         $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blog $blog)
    {
        try {
            $validatedData = $this->validate(
                $request,
                [
                    'title' => 'required|min:5|max:30',
                    'short_description' => 'required|min:10|max:80',
                    'description' => 'required',
                    'tags' => 'required',
                    'tags.*' => 'required'
                ]
            );

            // update blog data
            Blog::where('id', $blog->id)->update(
                [
                'title' => $request->title,
                'short_description' => $request->short_description,
                'description' => $request->description,
                ]
            );
            // update blog data

            // store blog tags data
            if ($request->tags) {
                $this->storeTags($request->tags, $blog->id);
            }

            return redirect()
                ->route('blog.show', [$blog->id])
                ->with(['status' => 'success','message'=>'Blog updated successfully !!']);
        } catch (Exception $e) {
            return redirect()->back()->with(['status' => 'danger','message' => $e->getMessage()])->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Blog $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog)
    {
        try {
            $blog->delete();
            return redirect()
                ->route('blog.index')
                ->with(['status' => 'success','message'=>'Blog Removed successfully !!']);
        } catch (Exception $e) {
            return redirect()->back()->with(['status' => 'danger','message' => $e->getMessage()])->withInput();
        }
    }

    /**
     * Get resources of the blog related tags.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllTags()
    {
        return BlogTags::select('tags')->groupBy('tags')->get();
    }

    /**
     * Remove the resources of the current blog and attach updated tags.
     *
     * @param \App\Models\BlogTags $tagData
     * @param \App\Models\Blog     $blogID
     */
    public function storeTags($tagData, $blogID)
    {
        BlogTags::where('blog_id', $blogID)->delete();
        foreach ($tagData as $tags) {
            BlogTags::create(
                [
                'blog_id' => $blogID,
                'tags' => $tags,
                ]
            );
        }
    }
}
