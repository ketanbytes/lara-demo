<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title','short_description','description'
    ];

    /***
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    // get all tags related to the blog
    public function allTags()
    {
        return $this->hasMany(BlogTags::class, 'blog_id');
    }
}
