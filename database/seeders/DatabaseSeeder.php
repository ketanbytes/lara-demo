<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades;
use DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // it is only to ensure to check with the large fake data
        $faker = Faker::create();
        for ($i=1; $i<=25; $i++) {
            $article=[
                'title'=> $faker->sentence(2),
                'short_description'=> $faker->sentence(4),
                'description'=> $faker->paragraph(150),
                'created_at'=> date('Y-m-d h:i:s'),
                'updated_at'=> date('Y-m-d h:i:s')];
            DB::table('blogs')->insert($article);
        }
    }
}
