@extends('layouts.app')
@section('title') {{'Save Blog'}} @endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Save Blog') }} <a href="{{route('blog.index')}}" class="btn btn-primary float-right">Back to Blog List</a></div>                

                <div class="card-body">

                    <form method="POST" action="{{ isset($blog) ? route('blog.update',$blog->id) : route('blog.store')}}" id="blog-form">
                    @if(isset($blog))
                    {{ method_field('PATCH') }}
                    @endif
                    @csrf      

                        <div class="form-group row">
                            <label for="name" class="col-md-2 col-form-label text-md-right">{{ __('Title') }}*</label>
                            <div class="col-md-8">
                                <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ isset($blog->title) ? $blog->title : old('title') }}" required autofocus placeholder="Blog Title">
                                @error('title')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="short_description" class="col-md-2 col-form-label text-md-right">{{ __('Short Description') }}*</label>
                            <div class="col-md-8">
                                <textarea id="short_description" class="form-control @error('short_description') is-invalid @enderror" name="short_description" required autofocus rows="6" placeholder="Blog Short Description">{{ isset($blog->short_description) ? $blog->short_description : old('short_description') }}</textarea>
                                @error('short_description')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>   

                        <div class="form-group row">
                            <label for="description" class="col-md-2 col-form-label text-md-right">{{ __('Description') }}*</label>
                            <div class="col-md-8">
                                <textarea id="description" class="form-control @error('description') is-invalid @enderror" name="description" required autofocus rows="6" placeholder="Blog Description">{{ isset($blog->description) ? $blog->description : old('description') }}</textarea>
                                @error('description')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>  

                        <div class="form-group row">
                            <label for="tags" class="col-md-2 col-form-label text-md-right">{{ __('Tags') }}*</label>
                            <div class="col-md-8">
                                <select class="form-control @error('tags') is-invalid @enderror" multiple="multiple" name="tags[]" id="tags">                                    
                                    @forelse($allTags as $tag)                                        
                                        <option value="{{$tag->tags}}" @if(!empty($blog->selectedTags)) @if(in_array($tag->tags,$blog->selectedTags)) selected @endif  @endif>{{$tag->tags}}</option>
                                    @empty
                                    <option>New Blog</option>
                                    <option>Technology</option>                                
                                    @endforelse      
                                </select>                                                                
                            </div>                            
                        </div>  

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-2">
                                <button type="submit" class="btn btn-success"">
                                    {{ __('Save Blog') }}
                                </button>
                                <a href="{{route('blog.index')}}" class="btn btn-danger">Back to List</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js_script')
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.full.min.js'></script>

<script type="text/javascript">
$(document).ready(function() {
    $('#tags').select2({
     tags: true
    });

    $("#blog-form").validate({
        rules: {
            title: {
                required: true,
                minlength:5,
                maxlength:30
            },
            short_description: {
                required: true,
                minlength:10,
                maxlength:80
            },
            description: {
                required: true,
                minlength:5,
            },
            'tags[]': {
               required: true,
           }
       },
       messages : {
        title: {
            required: "Title is required",    
            minlength: "Title must be at least 5 characters long",
            maxlength: "Maximum 30 characters allowed",
        },
        short_description: {
            required: "Short description is required",      
            minlength: "Short description must be at least 10 characters long",
            maxlength: "Maximum 80 characters allowed",
        },
        description: {
            required: "Description is required",      
            minlength: "Description must be at least 5 characters long",            
        },
    }});
});
</script>
@endpush

@push('css_list')
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css'>
<style>
#tags {
  width: 100%;
}
</style>
@endpush