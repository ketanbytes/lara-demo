@extends('layouts.app')
@section('title') {{'Blog List'}} @endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            @if (session('status'))
            <div class="alert alert-{{session('status')}}" role="alert">
                {{ session('message') }}
            </div>
            @endif                            

            <div class="card-columns row m-10">
                @forelse($blogs as $key => $blog)
                <div class="card @if($key+1 === 1) col-md-12 @endif" style="width: 18rem;">
                    <div class="card-body">                        
                        <h5 class="card-title @if($key+1 === 1) first_blog @endif">{{$blog->title}}</h5>                        
                        <h6 class="card-subtitle mb-2 text-muted"><i class="fa fa-clock-o" title="Publish Date"></i> {{$blog->created_at->diffForHumans()}}</h6>
                        <hr/>
                        <p class="card-text">{!! htmlentities(substr(strip_tags(trim(preg_replace('/<[^>]*>/', ' ',@$blog->short_description))),  0, 20),ENT_QUOTES | ENT_IGNORE, "UTF-8") !!}</p></p>
                        
                        @if (count($blog->allTags) > 0)
                            <p><strong>Tags:</strong>
                                @foreach($blog->allTags as $TagKey => $tagData)
                                    <span class="badge badge-success">{{ $tagData->tags }}</span>
                                @endforeach
                            </p>
                        @endif
                    </div>
                    <div class="card-footer bg-transparent border-success">
                        <a href="{{route('blog.show',$blog->id)}}" class="card-link">
                            <i class="fa fa-eye" title="update blog detail page"></i> View Blog            
                        </a>
                    </div>
                </div>         
                @empty
                    <h3 class="text-center">No Blog Found !!</h3>                    
                @endforelse
            <div>
        </div>
    </div>
</div>
@endsection
@push('css_list')
<style>
    .first_blog {
        font-size: 22px;
    }
    .card-columns {
        margin: 30px;
    }
</style>
@endpush
