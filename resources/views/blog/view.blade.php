@extends('layouts.app')
@section('title') {{$blog->title}} @endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
             @if (session('status'))
                <div class="alert alert-{{session('status')}}" role="alert">
                    {{ session('message') }}
                </div>
            @endif                            

            <div class="card margin_area">
                <div class="card-header"><strong>Blog Detail</strong>
                <a href="{{route('blog.index')}}" class="btn btn-primary float-right margin_area">Back to Blog List</a>
                <a href="{{route('blog.edit',$blog->id)}}" class="float-right btn btn-primary margin_area">
                    <i class="fa fa-pencil" title="update blog detail page"></i> Update Blog          
                </a>
            </div>

                <div class="card-body">
                    <div class="form-group row">
                        <label for="name" class="col-md-2 text-md-right"><strong>{{ __('Title') }}:</strong></label>
                        <div class="col-md-8">
                            {{$blog->title}}
                        </div>

                    <form action="{{ route('blog.destroy', $blog->id)}}" method="POST" onsubmit="return confirm('Do you really want to remove this blog?');">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-sm btn-danger"><i class="fa fa-remove"></i>&nbsp;Remove Blog</button>
                        </form>
                    </div>

                    <div class="form-group row">
                        <label for="name" class="col-md-2 text-md-right"><strong>{{ __('Short Description') }}:</strong></label>
                        <div class="col-md-8">
                            {{$blog->short_description}}
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="name" class="col-md-2 text-md-right"><strong>{{ __('Description') }}:</strong></label>
                        <div class="col-md-8">
                            {{$blog->description}}
                        </div>
                    </div>

                    @if($blog->allTags->toArray())
                    <div class="form-group row">
                        <label for="name" class="col-md-2 text-md-right"><strong>{{ __('Tags') }}:</strong></label>
                        <div class="col-md-8">
                            @foreach($blog->allTags as $TagKey => $tagData)
                                <span class="badge badge-success">{{ $tagData->tags }}</span>
                            @endforeach
                        </div>
                    </div>
                    @endif

                    @if($blog->created_at)
                    <div class="form-group row">
                        <label for="name" class="col-md-2 text-md-right"><strong>{{ __('Publish Date') }}:</strong></label>
                        <div class="col-md-8">
                            {{$blog->created_at->diffForHumans()}}
                        </div>
                    </div>
                    @endif

                    @if($blog->updated_at)
                    <div class="form-group row">
                        <label for="name" class="col-md-2 text-md-right"><strong>{{ __('Last Updated Date') }}:</strong></label>
                        <div class="col-md-8">
                            {{$blog->updated_at->diffForHumans()}}
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('css_list')
<style>
.margin_area{
    margin: 5px;
}
</style>
@endpush